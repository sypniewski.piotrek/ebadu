def calkinWilfSequence(number):
    def fractions():
        lst_nums = [[1, 1]]
        yield lst_nums[-1]
        it, last_root = 0, 0
        while True:
            it += 1
            lst_nums.append(
                [lst_nums[last_root][0], lst_nums[last_root][0]+lst_nums[last_root][1]])
            yield lst_nums[-1]
            if it == 2:
                it = 0
                last_root += 1
            lst_nums.append(
                [lst_nums[last_root][0]+lst_nums[last_root][1], lst_nums[last_root][1]])
            yield lst_nums[-1]
            it += 1
            if it == 2:
                it = 0
                last_root += 1

    gen = fractions()
    res = 0
    while next(gen) != number:
        res += 1
    return res


def calkinWilfSequence1(number):
    def fractions():
        a = b = 1
        while True:
            yield [a, b]
            a, b = b, 2 * (a - a % b) + b - a

    gen = fractions()
    res = 0
    while next(gen) != number:
        res += 1
    return res


if __name__ == "__main__":
    print(calkinWilfSequence1([1, 3]))
    print(calkinWilfSequence([1, 2]))
